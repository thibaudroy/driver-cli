# Repository information
REPO=$(strip $(call check-repo))
PROJECT=$(strip $(call check-project))
TAG=$(shell git tag --points-at HEAD)
VERSION=$(if $(TAG),$(TAG),unreleased)
REVISION=$(strip $(shell git rev-parse HEAD))

# Golang general settings
GOLANG_VERSION=$(shell sed -n 's/^go //p' ./go.mod)
GOPATH?=$(shell echo $(HOME)/go)
GOBIN?=$(shell echo $(GOPATH)/bin)

# Module binaries and packages
MODULE=$(shell go list -m)
PACKAGES=$(shell go list ./...)
BINARIES?=$(if $(CMD_DIR),$(notdir $(wildcard $(CMD_DIR)/*)),$(basename $(wildcard ./*.go)))

# Build flags
LDFLAGS=-X $(BUILD_HASH).ProjectName=$(PROJECT)\
		$(if $(REVISION),-X $(BUILD_HASH).Revision=$(REVISION),)\
		$(if $(VERSION),-X $(BUILD_HASH).Version=$(VERSION),)

# Default command
DEFAULT: build

# Configuration files
include config.mk
-include config.override.mk

## <bin>: build a specific binary from cmd/<bin> subfolder
.PHONY: $(BINARIES)
$(BINARIES): check vendor
	$(call build-binary,$@)

## build: generate enclosed binaries (default target)
.PHONY: build
build: $(BUILD_PREREQUISITES)
ifneq ($(strip $(BINARIES)),)
	$(if $(BINARIES),$(call build-all),$(call build-binary,$(PROJECT)))
endif

define build-all
	$(foreach bin,$(BINARIES),$(call build-binary,$(bin)))
endef

define build-binary
	$(info > Compiling $(1))
	@go build -ldflags '$(LDFLAGS)' $(GOLANG_BUILD_EXTRA_ARGS) -o $(BUILD_DIR)/$(1) $(if $(CMD_DIR),$(CMD_DIR)/$(1)/*.go,)
	@md5sum < $(BUILD_DIR)/$(1) | cut -d ' ' -f 1 > $(BUILD_DIR)/$(1).md5.txt
	$(echo "")
endef

.PHONY: build-debug
build-debug: $(BUILD_PREREQUISITES)
ifneq ($(strip $(BINARIES)),)
	$(if $(BINARIES),$(call build-all-debug),$(call build-binary-debug,$(PROJECT)))
endif

define build-all-debug
	$(foreach bin,$(BINARIES),$(call build-binary-debug,$(bin)))
endef

define build-binary-debug
	$(info > Compiling $(1) debug)
	@go build -gcflags "all=-N -l" -ldflags '$(LDFLAGS)' $(GOLANG_BUILD_EXTRA_ARGS) -o $(BUILD_DIR)/$(1) $(if $(CMD_DIR),$(CMD_DIR)/$(1)/*.go,)
	@md5sum < $(BUILD_DIR)/$(1) | cut -d ' ' -f 1 > $(BUILD_DIR)/$(1).md5.txt
	$(echo "")
endef


## install: install the package locally
.PHONY: install
install: $(INSTALL_PREREQUISITES)
ifneq ($(strip $(BINARIES)),)
	$(if $(BINARIES),$(call install-all),$(call install-binary,$(PROJECT)))
endif

define install-all
	$(foreach bin,$(BINARIES),$(call install-binary,$(bin)))
endef

define install-binary
	$(info > Installing $(1) in $(GOBIN))
	@env GOOS=$(GOOS) GOARCH=$(GOARCH) CGO_ENABLED=$(CGO) go build -ldflags '-s -w -extldflags "-static" $(LDFLAGS)' $(GOLANG_BUILD_EXTRA_ARGS) -o $(GOBIN)/$(1) $(if $(CMD_DIR),$(CMD_DIR)/$(1)/*.go,)
endef

.PHONY: install-debug
install-debug: $(INSTALL_PREREQUISITES)
ifneq ($(strip $(BINARIES)),)
	$(if $(BINARIES),$(call install-all-debug),$(call install-binary-debug,$(PROJECT)))
endif

define install-all-debug
	$(foreach bin,$(BINARIES),$(call install-binary-debug,$(bin)))
endef

define install-binary-debug
	$(info > Installing $(1) debug in $(GOBIN))
	@env GOOS=$(GOOS) GOARCH=$(GOARCH) CGO_ENABLED=$(CGO) go build -gcflags "all=-N -l" -ldflags '-extldflags "-static" $(LDFLAGS)' $(GOLANG_BUILD_EXTRA_ARGS) -o $(GOBIN)/$(1) $(if $(CMD_DIR),$(CMD_DIR)/$(1)/*.go,)
endef

## package: compile package for target OS
.PHONY: package
package: $(INSTALL_PREREQUISITES)
ifneq ($(strip $(BINARIES)),)
	$(if $(CMD_DIR),\
	$(foreach bin,$(BINARIES),$(foreach os,$(OS),$(foreach arch,$(ARCH),$(call build-package,$(bin),$(os),$(arch))))),\
	$(foreach os,$(OS),$(foreach arch,$(ARCH),$(call build-package,$(PROJECT),$(os),$(arch)))))
endif

define build-package
	$(info > Packaging $(1) for $(2) $(3))
	@env GOOS=$(2) GOARCH=$(3) go build -ldflags '$(LDFLAGS)' $(GOLANG_BUILD_EXTRA_ARGS) -o $(1) $(if $(CMD_DIR),$(CMD_DIR)/$(1)/*.go,)
	$(if $(filter $(1),windows),@zip $(BUILD_DIR)/$(1)_$(2)_$(3).zip $(1).exe,@tar cf $(BUILD_DIR)/$(1)_$(2)_$(3).tar $(1))
	@md5sum < $(BUILD_DIR)/$(1)_$(2)_$(3).tar | cut -d ' ' -f 1 > $(BUILD_DIR)/$(1)_$(2)_$(3).tar.md5.txt
	@rm $(1)$(if $(filter $(1),windows),.exe,)
endef

## test: run unit and end-to-end tests
.PHONY: test
test: $(TEST_PREREQUISITES)
	$(info > Running unit tests)
	@go test $(PACKAGES) $(GOLANG_TEST_FLAGS) $(GOLANG_TEST_EXTRA_ARGS);

## cover: verify the test coverage of the current module
.PHONY: cover
cover: $(TEST_PREREQUISITES)
	$(info > Running code coverage)
	@go test $(PACKAGES) $(GOLANG_TEST_FLAGS) -tags 'unit e2e' -coverprofile=/tmp/coverage.out;
	$(if $(filter $(OPTIONS),html),,$(shell go tool cover -html=/tmp/coverage.out))

## doc: generate documentation automatically from source code
.PHONY: doc
doc: $(DOC_PREREQUISITES)
	$(info > Generating documentation)
	@docker run --rm -v $(HOME)/.netrc:/home/user/.netrc -v $(PWD):/src --entrypoint /bin/sh \
    564425383837.dkr.ecr.eu-west-3.amazonaws.com/go-docsf:$(GODOCSF_VERSION) \
    -c 'cd /src && go-docsf build -o $(GODOCSF_OUTPUT) -i'

## doc-check: check that the documentation is up-to-date
.PHONY: doc-check
doc-check: $(DOC_PREREQUISITES)
	$(info > Checking documentation)
	@docker run --rm -v $(HOME)/.netrc:/home/user/.netrc -v $(PWD):/src --entrypoint /bin/sh \
    564425383837.dkr.ecr.eu-west-3.amazonaws.com/go-docsf:$(GODOCSF_VERSION) \
    -c 'cd /src && go-docsf check -o $(GODOCSF_OUTPUT)'

## gofmt: apply go fmt on packages
.PHONY: gofmt
gofmt: vendor
	$(info > Formatting code )
	$(foreach pkg,$(PACKAGES),$(call gofmt-pkg,$(pkg)))
	@echo "gofmt success\n"

define gofmt-pkg
	@echo "> Checking $(1)"
	$(eval FILES=$(shell go list -f '{{range .GoFiles}}{{$$.Dir}}/{{.}} {{end}}' $(1)))
	$(if $(FILES),,$(call gofmt-files,$(FILES)))
endef

define gofmt-files
	$(eval OUTPUT=$(shell gofmt -w -s $(1) 2>&1;))
	$(if $(OUTPUT),$(info > $(OUTPUT)),$(info > Go fmt failure))
endef

## govet: run linters, see github.com/golangci/golangci-lint
.PHONY: govet
govet: vendor
	$(if $(filter $(GOVET),TRUE),$(call govet-check),)

define govet-check
	$(eval CI_LINT=$(shell which golangci-lint))
	$(if $(CI_LINT),$(call govet-run),\
	$(info > Skip - golangci-lint is not installed. Please see https://github.com/golangci/golangci-lint#install for installation instructions.))
endef

define govet-run
	$(info > Applying linting rules)
	@golangci-lint run ./...;
	@echo "govet success"
endef

## check: verify the target repository is a go module
.PHONY: check
check:
	$(call check-module)

## format: apply formatting and linting rules
.PHONY: format
format: gofmt govet
ifeq ($(strip $(BINARIES)),)
	$(info > No binary found)
else
	$(call check-dir,$(BUILD_DIR))
endif

## vendor: download third-party packages and modules
.PHONY: vendor
vendor: tidy
	$(info > Downloading dependencies)
	$(call check-dir,$@)
	@go mod vendor

## tidy: cleanup, order and verify dependencies
.PHONY: tidy
tidy:
	$(info > Verifying dependencies)
	@go mod tidy
	@go mod verify

# URL and path checkers with extended regex options
define check-repo
	$(eval V=$(shell go list -m | sed 's/\(.*\)\/\(.*\)\/\(.*\)/\3/' | grep -E '^\v[0-9]'))
	$(join https://,$(if $(V),$(subst /$(V),,$(MODULE)),$(MODULE)))
endef

define check-project
	$(eval V=$(shell go list -m | sed 's/\(.*\)\/\(.*\)\/\(.*\)/\3/' | grep -E '^\v[0-9]'))
	$(if $(V),$(shell basename $(dir $(MODULE))),$(notdir $(MODULE)))
endef

define check-dir
	$(if $(filter $(wildcard $1/*),),mkdir -p $1,)
endef

define check-module
	@if [ -f "go.mod" ]; then\
		echo -n "> Using go.mod\n";\
	else\
		echo -n "> This is not a Go module. Run 'go init' first.\n";\
		echo -n "  See https://golang.org/doc/tutorial/create-module\n\n";\
		exit 1;\
	fi
endef

define check-install
	@if  [ -f "$(1)/$(2)" ]; then\
		echo -n "> Removing $(2) from $(1)\n";\
		rm -rf $(1)/$(2);\
	else\
		echo -n "> $(2) not installed yet\n";\
	fi
endef

## clean: empty local build and cache folders
.PHONY: clean
clean: clean-cache clean-test-cache

## clean-all: empty build and cache folders
.PHONY: clean-all
clean-all: clean clean-mod-cache clean-build clean-install

clean-build:
	$(info > Removing binaries)
	$(if $(filter $(wildcard $(BUILD_DIR)),),,@rm -rf $(BUILD_DIR)/*)

clean-cache:
	$(info > Removing local cache)
	@go clean -cache

clean-install:
	$(foreach bin,$(BINARIES),$(call check-install,$(GOBIN),$(bin)))
	@echo ""

clean-mod-cache:
	$(info > Removing session golang module cache)
	@go clean -modcache -i -r

clean-test-cache:
	$(info > Removing test cache)
	@go clean -testcache ./...

## info: display module information and package list
.PHONY: info
info: check
	$(info Project:	$(PROJECT))
	$(info Repository:	$(REPO))
	$(info Version:	$(VERSION))
	$(info Revision:	$(REVISION))
	$(info Compiler:	$(GOLANG_VERSION))
	$(call show,$(BINARIES),Binaries)
	@echo ""

# Display options
define show
	$(if $(filter $(call count,$(1)),0),,$(info $(2):	[$(strip $(call count,$(1)))]))
	$(foreach item, $(sort $(1)),$(info $(NULL)		- $(notdir $(item))))
endef

define count
	$(eval counter=0)
	$(foreach item,$(1),$(eval counter:=$(shell echo $$(($(counter)+1)))))$(counter)
endef

## help: print this help message
.PHONY: help
help:
	@echo "Usage: \n"
	$(call get-help,Makefile)
	$(if $(filter ./config.override.mk,),$(call get-help,config.override.mk))
	@echo ""

define get-help
	@sed -n 's/^##//p' $(1) | column -t -s ':' | sed -e 's/^/ /'
endef
