# Buf settings
BUF_VERSION:=v1.31.0


## grpc-lint: lint gRPC proto files
.PHONY: grpc-lint
grpc-lint:
	go run github.com/bufbuild/buf/cmd/buf@$(BUF_VERSION) lint

## grpc-gen: generate gRPC code from proto files
.PHONY: grpc-gen
grpc-gen:
	go run github.com/bufbuild/buf/cmd/buf@$(BUF_VERSION) generate
