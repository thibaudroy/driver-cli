# Do not modify this file, if you want to configure your own environment copy
# this file in config.override.mk and modify that file, or defining environment
# variables using the same names found here.

# Default binary and build settings
CMD_DIR?=cmd
BUILD_DIR?=bin
BUILD_HASH?=gitlab.com/agregio_group/control/library/common/v2/pkg/version

# Package settings
OS?=linux darwin windows
ARCH?=amd64

# Golang settings
GO111MODULE?=auto
GONOPROXY?=none
GOPRIVATE?=gitlab.com/agregio_group
GOVET?=
GOOS?=linux
GOARCH?=amd64
CGO_ENABLED?=0

# Test variables to enrich unit tests
GOLANG_TEST_CACHE?=/tmp/cache
GOLANG_TEST_FLAGS?=-mod=vendor -timeout 30s -race
GOLANG_TEST_EXTRA_ARGS?=

# Documentation settings
GODOCSF_VERSION?=v0.9.4
GODOCSF_OUTPUT?=docs/code
GODOCSF_TARGET_BRANCH?=main

# Recipe pre-requisites
BUILD_PREREQUISITES?=check vendor
INSTALL_PREREQUISITES?=check format vendor
TEST_PREREQUISITES?=check format vendor
DOC_PREREQUISITES?=vendor
