# Contributing

[[_TOC_]]

## Pre-Commit

1. Install [pre-commit](https://pre-commit.com).

1. Enable pre-commit hooks

   ```shell
   pre-commit install
   ```

1. (Optional) Force check

   ```shell
   pre-commit run --all-files
   ```

## Documentation

1. Don't forget to update the documentation before committing your changes

   ```shell
   make doc
   ```

## Administration interface

To add a service to `admin` gRPC interface, follow these steps:

1. enrich `proto/admin/v<X>/admin.proto` with the `rpc` service and necessary `message`. See
   [protobuf programming guide](https://protobuf.dev/programming-guides/proto3)

1. verify the syntax with `make grpc-lint`

1. generate the Go code using [buf](https://buf.build):

   1. run `buf mod update` to update dependencies
   1. run `make grpc-gen`
   1. generated Go code and gRPC/HTTP gateway Swagger file are updated under `proto/admin/v<X>/`

1. implement service on server and client side in `internal/admin` package
